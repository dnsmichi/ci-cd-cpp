# ci-cd-cpp

This repository is a playground to add C++ builds with CMake and ccache for GitLab CI/CD.

## Requirements

- Builder Image with ccache installed
- Evaluate custom settings for ccache and cmake
- Add cache to .gitlab-ci.yml

## Build

```
test -d build || mkdir -p build
cd build; cmake ..; cd ..

make -C build
```

## Vendor Subtrees

```
git subtree add --prefix vendor/build-docker-scripts https://git.icinga.com/build-docker/scripts.git HEAD --squash

git subtree add --prefix vendor/build-docker-centos https://git.icinga.com/build-docker/centos.git HEAD --squash
```

