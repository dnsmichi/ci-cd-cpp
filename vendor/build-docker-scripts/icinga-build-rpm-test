#!/bin/bash

set -e
# shellcheck source=functions-rpm.sh
. "$(dirname "$(readlink -f "$0")")"/functions-rpm.sh

repository="${WORKDIR}/${BUILDDIR}"/RPMS

# Preparing local repository
case "$ICINGA_BUILD_OS" in
  centos|fedora|rhel)
    (
      set -ex
      createrepo "$repository"

      sudo tee /etc/yum.repos.d/local.repo <<REPO
[local]
name=local workspace
baseurl=file://$repository
enabled=1
gpgcheck=0
REPO
    )
    ;;
  sles|opensuse*)
    (
      set -ex
      #sudo zypper --non-interactive clean --all
      run-retry-timeout sudo zypper --non-interactive --no-gpg-checks --gpg-auto-import-keys ref -fs

      # Just use the local dir, zypper will take care of the rest!
      if ! sudo zypper lr | grep -q "$repository"; then
        sudo zypper addrepo --no-gpgcheck --refresh "$repository" local
      fi
    )

    # Workaround for SLES 11 SP4
    # https://github.com/Icinga/puppet-icinga_build/issues/33
    if [[ "$ICINGA_BUILD_DIST" = 11.* ]]; then
      # when the Kernel has at least some SELinux code loaded (e.g. on CoreOS)
      # libselinux1 sees the process having an context, and assumes SELinux is enabled
      sudo sh -xe <<<"
        getent group icinga >/dev/null || echo 'icinga:!:998:' >> /etc/group
        getent group icingacmd >/dev/null || echo 'icingacmd:!:997:icinga' >> /etc/group
        getent group icingaweb2 >/dev/null || echo 'icingaweb2:!:996:' >> /etc/group
        if ! getent passwd icinga >/dev/null; then
          echo 'icinga:x:998:998:icinga:/var/spool/icinga2:/sbin/nologin' >> /etc/passwd
          echo 'icinga:!:17310:0:99999:7:::' >> /etc/shadow
        fi
      "
    fi
    ;;
  *)
    echo "OS $ICINGA_BUILD_OS not supported in testing..." >&2
    exit 1
    ;;
esac

test_script=
if [ "$#" -gt 0 ]; then
  test_script="$*"
elif [ -f "icinga-build-test" ]; then
  test_script="./icinga-build-test"
elif [ -f "${WORKDIR}/testing/start_test.sh" ]; then
  test_script="${WORKDIR}/testing/start_test.sh"
fi

echo
if [ -n "${test_script}" ]; then
  echo "[ Running script: ${test_script} ]"
  ${test_script}
else
  echo "[ Falling back to pure package installation ]"
  icinga-build-rpm-install "${ICINGA_BUILD_PROJECT}"
fi
