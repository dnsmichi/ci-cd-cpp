#!/bin/bash

set -e
# shellcheck source=functions-rpm.sh
. "$(dirname "$(readlink -f "$0")")"/functions-rpm.sh

source_rpm="$(ls "${BUILDDIR}"/SRPMS/*.src.rpm)"

if [ "$(echo "${source_rpm}" | wc -l)" -gt 1 ]; then
  echo "More than one spec file found:" >&2
  ls -al "${BUILDDIR}/SRPMS" >&2
  exit 1
fi

echo "[ Update package cache ]"
case "$ICINGA_BUILD_OS" in
  opensuse*|sles)
    (
      set -ex
      # Note: force rebuilding here, because zypper is a bit dumb
      run-retry-timeout sudo zypper --non-interactive clean --all
      run-retry-timeout sudo zypper --non-interactive --no-gpg-checks --gpg-auto-import-keys ref -fs
    ) || exit 1
    ;;
  *)
    (
      if command -v dnf &>/dev/null; then
        set -ex
        run-retry-timeout sudo dnf clean expire-cache
        run-retry-timeout sudo dnf makecache
      else
        set -ex
        run-retry-timeout sudo yum clean expire-cache
        run-retry-timeout sudo yum makecache fast
      fi
    ) || exit 1
    ;;
esac

echo "[ Installing build dependencies ]"
case "$ICINGA_BUILD_OS" in
  opensuse*|sles)
    (
      set -ex
      # shellcheck disable=SC2046
      run-retry-timeout sudo zypper --non-interactive install $(rpm -qpR "${source_rpm}")
    ) || exit 1
    ;;
  *)
    (
      if command -v dnf-builddep >/dev/null; then
        set -ex
        run-retry-timeout sudo dnf-builddep -y "${source_rpm}"
      else
        set -ex
        run-retry-timeout sudo yum-builddep -y "${source_rpm}"
      fi
    ) || exit 1
    ;;
esac

if command -v ccache &>/dev/null; then
  echo "[ Preparing ccache for our environment ]"
  export CCACHE_DIR="${WORKDIR}/ccache"
  rm -f ccache.stats
  preconfigure_ccache

  # reset ccache statistics
  # so we now the statistics of the new build
  if [ -d "${CCACHE_DIR}" ]; then
    ccache -z # (--zero-stats)
  fi
fi

echo "[ Building binary package ]"
eval "$(get_rpmbuild --rebuild "${source_rpm}")"
(
  set -ex
  # explicitly unset arch, it may conflict with internal scripts
  # of the tools we build (happens with Icinga 2 and wxWidgets)
  unset arch

  if [ -n "${ICINGA_BUILD_DEVTOOLSET_2:-}" ] && [ -e /opt/rh/devtoolset-2 ]; then
    # Run in a newer compiler environment
    # Environment needs to be set here for ccache to find the compiler to use...
    scl enable devtoolset-2 -- "${RPMBUILD[@]}"
  else
    "${RPMBUILD[@]}"
  fi
) || exit 1

if command -v ccache &>/dev/null && [ -d "${CCACHE_DIR}" ]; then
  cached_files="$(find "${CCACHE_DIR}" -type f ! -name ccache.conf ! -name stats | wc -l)"
  if [ "$cached_files" -eq 0 ]; then
    echo "Removing empty ccache dir: ${CCACHE_DIR}"
    rm -rf "${CCACHE_DIR}"
  else
    echo "[ ccache stats ]"
    ccache -s | tee build/ccache-stats.txt # (--show-stats)
    echo
  fi
fi

echo "[ Running rpmlint ]"
(
  if [ -f /etc/os-release ]; then
    source /etc/os-release
  fi
  if [ "$ID" = sles ] && [[ "$VERSION" = 11.* ]]; then
    echo "Skip rpmlint..."
    exit 0
  fi
  lintrc=0
  for file in "${BUILDDIR}"/RPMS/**/*.rpm; do
    lintconf=(--info --verbose -o "NetworkEnabled True")
    pkg_name="$(basename "$file" | sed -E 's/-[^-]+-[^-]+$//')"
    if [ -e rpmlint/"${pkg_name}.conf" ]; then
      lintconf+=(-f rpmlint/"${pkg_name}.conf")
    fi
    (
      set -xo pipefail
      rpmlint "${lintconf[@]}" "${file}" | tee "${BUILDDIR}/rpmlint-${pkg_name}.log"
    ) || lintrc=$?
  done
  [ "$lintrc" -eq 0 ]
) || [ "${ICINGA_BUILD_IGNORE_LINT}" -eq 1 ] || exit 1
