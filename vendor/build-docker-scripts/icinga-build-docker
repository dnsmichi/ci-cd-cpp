#!/bin/bash

set -e

## config
: "${ICINGA_DOCKER_REGISTRY:="registry.icinga.com"}"
: "${ICINGA_DOCKER_ORG:="build-docker"}"
: "${ICINGA_DOCKER_PULL:=1}"
: "${ICINGA_SCRIPT_DEVEL:=0}"

SCRIPT_HOME="$(dirname "$(readlink -f "$0")")"

## functions
usage() {
  echo "$0 <target>"
  echo
}

if [ -n "$1" ]; then
  TARGET="$1"
  shift
else
  echo "Target is missing!" >&2
  exit 1
fi

DOCKER_IMAGE="${ICINGA_DOCKER_ORG}/${TARGET}"
if [ -n "${ICINGA_DOCKER_REGISTRY}" ]; then
  DOCKER_IMAGE="${ICINGA_DOCKER_REGISTRY}/${DOCKER_IMAGE}"
fi

if [ "$ICINGA_DOCKER_PULL" = 1 ]; then
  echo "[ Pulling Docker image ]"
  ( set -ex; docker pull "${DOCKER_IMAGE}" )
fi

dockeropt=()
[ ! -t 0 ] || [ ! -t 1 ] || dockeropt+=('-t')

if [ "${ICINGA_SCRIPT_DEVEL}" -eq 1 ]; then
  echo "Running in ICINGA_SCRIPT_DEVEL mode, mounting scripts into container"
  dockeropt+=(-v "${SCRIPT_HOME}":/usr/local/bin:ro)
fi

env_file="$(mktemp)"
trap 'rm -f ${env_file}' EXIT INT

env | grep -e ^ICINGA > "${env_file}" || true
env | grep -e ^APTLY >> "${env_file}" || true
env | grep -e ^CI_ >> "${env_file}" || true
env | grep -e ^USCAN_ >> "${env_file}" || true

echo "[ Running build in Docker ]"
set -ex
docker run -i --rm "${dockeropt[@]}" \
  --env-file "${env_file}" \
  -v "$(pwd)":/tmp/build \
  --workdir /tmp/build \
  "${DOCKER_IMAGE}" \
  "$@"

# vi: ts=2 sw=2 expandtab
