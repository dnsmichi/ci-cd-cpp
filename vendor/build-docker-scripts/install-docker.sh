#!/bin/bash
#
# This script helps us to prepare a Docker host for the build system
#
# It is used with Docker Machine to install Docker, plus addons
#
# See --engine-install-url at docker-machine create --help

set -e

run() {
  (set -x; "$@")
}

sudo mkdir -p /etc/docker

cat <<EOF |sudo tee /etc/docker/daemon.json >/dev/null
{
  "registry-mirrors": ["https://mirror.gcr.io"]
}
EOF

echo "Installing Docker via get.docker.com"
run curl -LsS https://get.docker.com -o /tmp/get-docker.sh
run sh /tmp/get-docker.sh

echo "Installing QEMU and helpers"
run sudo apt-get update
run sudo apt-get install -y qemu-user-static binfmt-support
