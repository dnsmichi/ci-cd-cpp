#!/bin/sh
set -eu

rc=0

tmp="$(mktemp)"
trap 'rm -f ${tmp}' EXIT INT TERM

find . -maxdepth 1 -type f -perm +1 -or -name \*.sh >"${tmp}"

while IFS= read -r file
do
  shebang="$(head -n1 "$file")"
  echo "$shebang" | grep -qE "/(ba)?sh" || continue
  echo "Checking $file"
  if ! shellcheck -x "$file"; then
    rc=1
  fi
done < "${tmp}"

exit $rc
