Scripts for package builds in Docker
====================================

This scripts slowly replace all embedded scripts in [Puppet module icinga_build](https://github.com/Icinga/puppet-icinga_build).

Please note:
* This is for an internal build environment
* Image names are not best practice

Documentation can be found under the [docs repository](https://git.icinga.com/build-docker/docs).

## License

Icinga, all tools and documentation are licensed under the terms of the GNU
General Public License Version 2, you will find a copy of this license in the
COPYING file included in the source package.
